# What is this?

This is capek, a bot for managing which users can join an IRC channel. It references a pre-existing userbase configured in a (presently) MySQL database, keyed by separate usernames and bcrypt-encrypted passwords.

# Installing

capek needs the bcrypt, irc, minimist, and mysql modules and their dependencies. `npm install` in the repository root should be enough to install them. The bcrypt module needs a C compiler.

# Configuring

The bot requires a configuration file in JSON format. The configuration file consists of a top level JSON object with "irc" and "db" objects within it. The "irc" object requires the keys "ircNick", "ircInviteChannel", "ircStaffChannel", "ircHostname", and "ircPort". It additionally recognizes the "ircUseSSL", "ircStaffChannelKey", and "ircNickservPassword" keys. 