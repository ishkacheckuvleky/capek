var mysql = require('mysql');
var bcrypt = require('bcrypt');
var sprintf = require('util').format;

exports.capekDb = capekDb;

function capekDb(configData) {
	var self = this;
	for(k in configData) {
		self[k] = configData[k];
	}
	self.createUsersSql = 'CREATE TABLE users(id INT AUTO_INCREMENT PRIMARY KEY ' +
		', username VARCHAR(30), irckey CHAR(60), irc_banned TINYINT' +
		', irc_nickname VARCHAR(30));'
	self.checkUserSql = 'SELECT irckey, banned, irc_banned FROM users WHERE username = ? ' +
		'AND irckey IS NOT NULL';
	self.storeNickForUserSql = 'UPDATE users SET irc_nickname = ? WHERE ' +
		'username = ?';
	self.getNickForUserSql = 'SELECT irc_nickname FROM users WHERE username = ?';
	self.getUserForNickSql = 'SELECT username FROM users WHERE irc_nickname = ?';

	self.init = function(cb) {
		self.connect();
		self.pool.query(self.createUsersSql, function(err, result) {
			if(err != null) {
				cb(err, null);
			} else {
				cb(null, true);
			}
		});
	};

	self.connect = function() {
		if(!self.pool) {
			self.pool = mysql.createPool({
				host: self.dbHostname,
				socketPath: self.dbSocketPath,
				port: self.dbPort,
				user: self.dbUsername,
				password: self.dbPassword,
				database: self.dbDatabase,
				debug: true,
			});
		}
	};

	self.checkUser = function(userName, nick, passKey, cb) {
		self.pool.query(self.checkUserSql, [userName], function(err, rows) {
			if(err !== null) {
				cb(err, null);
			}
			else if(rows.length == 0) {
				cb(null, {
					noMatch: true,
					userName: userName,
				});
			}
			else {
				cb(null, {
					userName: userName,
					isBanned: rows[0].irc_banned || rows[0].banned,
					passwordMatch: bcrypt.compareSync(passKey, rows[0].irckey)
				});
			}
		})
	};

	self.storeNickForUser = function(nick, userName, cb) {
		self.pool.query(self.storeNickForUserSql, [nick, userName], function(err, result) {
			if(err !== null) {
				cb(err, null);
			}
			else if(result.affectedRows != 1) {
				var errorString = sprintf('%s/%s update affected %d rows', nick, userName,
					result.affectedRows);
				cb(new Error(errorString), {
					userName: userName,
					nick: nick
				})
			} else {
				cb(null, {
					userName: userName,
					nick: nick
				})
			}
		});
	};

	self.getUserForNick = function(nick, cb) {
		self.pool.query(self.getUserForNickSql, [nick], function(err, rows) {
			if(err !== null) {
				cb(err, null);
			}
			else {
				var users = [];
				rows.forEach(function (row, i, a) {
					console.dir(row);
					users.push(row.username);
				});
				cb(null, {
					nick: nick,
					users: users || []
				});
			}
		});
	};

	self.getNickForUser = function(userName, cb) {
		self.pool.query(self.getNickForUserSql, [userName], function(err, rows) {
			if(err !== null) {
				cb(err, null);
			}
			else {
				var nicks = [];
				rows.forEach(function (row, i, a) {
					console.dir(row);
					nicks.push(row.irc_nickname);
				});
				cb(null, {
					nicks: nicks || [],
					user: userName
				});
			}
		});
	}
}
