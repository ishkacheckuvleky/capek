var irc = require('irc');
var db = require('./db.js');
var sprintf = require('util').format;

exports.capekBot = capekBot;

function capekBot (configData) {
	var self = this;
	for(k in configData.irc) {
		self[k] = configData.irc[k];
	}
	self.hasRegistered = false;
	self.db = new db.capekDb(configData.db);
	self.irc = new irc.Client(
		self.ircHostname, self.ircNick, {
			userName: 'capek',
			realName: 'capek',
			port: self.ircPort,
			channels: [],
			secure: self.ircUseSSL,
			stripColors: true,
			debug: true,
			autoConnect: false,
		}
	);

	self.irc.addListener('error', function(message) {
		console.dir('Error: ' + message);
	});

	self.irc.addListener('motd', function() {
		self.irc.say('nickserv', 'identify ' + self.ircNickservPassword);
	});

	self.irc.addListener('nick', function(oldNick, newNick, channels, message) {
		if(channels.indexOf(self.ircInviteChannel) != -1) {
			self.db.getUserForNick(oldNick, function(err, result) {
				if(err !== null) {
					self.irc.say(self.ircStaffChannel, sprintf('Error looking up %s old user', oldNick));
				} else {
					var userName = result.users[0];
					if(userName) {
						self.db.storeNickForUser(newNick, userName, function(err, result) {
							if(err !== null) {
								self.irc.say(self.ircStaffChannel, 'Error storing new ' + nick + ' ' + userName + ' mapping');
							}
						});
					} else {
						self.irc.say(self.ircStaffChannel, 'Unknown miscreant ' + newNick);
					}
				}
			});
		}
	});

	self.irc.addListener('notice', function(nick, to, text, message) {
		if(nick == 'NickServ' && to == self.ircNick && !self.hasRegistered
			&& text == 'Password accepted - you are now recognized.') {
				console.log('Identified to nickserv -- joining');
				self.irc.join(self.ircInviteChannel, function(channel, nick, message) {
				});

				self.irc.join(self.ircStaffChannel + ' ' + self.ircStaffChannelKey, function(channel, nick, message) {
				});
				self.hasRegistered = true;
			}
	});

	self.irc.addListener('message' + self.ircStaffChannel, function(nick, text, message) {
		var words = text.split(/ /);
		var command = words[0];
		var arg = words[1];
		if(command == '!user2nick') {
			self.db.getNickForUser(arg, function(err, result) {
				if(err !== null) {
					self.irc.say(self.ircStaffChannel, 'Error: ' + err.message);
				} else {
					var nicks = result.nicks.join(' ');
					self.irc.say(self.ircStaffChannel, arg + ' has these nicks: ' + nicks);
				}
			});
		} else if(command == '!nick2user') {
			self.db.getUserForNick(arg, function(err, result) {
				if(err !== null) {
					self.irc.say(self.ircStaffChannel, 'Error: ' + err.message);
				} else {
					var users = result.users.join(' ');
					self.irc.say(self.ircStaffChannel, arg + ' has these usernames: ' + users);
				}
			});
		}
	});

	self.irc.addListener('pm', function(nick, text, message) {
		var words = text.split(' ', 3);
		var command = words[0].toLowerCase();
		var userName = words[1];
		var passKey = words[2];
		if(command != 'identify') {
			self.irc.say(nick, 'Usage: identify [site username] [IRC key]');
		} else {
			self.db.checkUser(userName, nick, passKey, function(err, result) {
				if(err !== null && result == null) {
					self.irc.say(nick, 'Error: report to ish');
					self.irc.say(self.ircStaffChannel, 'Error ' + err + ' for ' + nick);
				} else {
					if(result.isBanned) {
						self.irc.say(nick, 'You are banned.');
					} else if(!result.passwordMatch) {
						self.irc.say(nick, 'Invalid IRC key.');
						if(!result.noMatch) {
							self.irc.say(self.ircStaffChannel, 'Invalid auth attempt for ' + nick + ' with ' + userName);
						} else {
							self.irc.say(self.ircStaffChannel, 'No match for for ' + nick + ' with ' + userName);
						}
					} else {
						self.irc.send('INVITE', nick, self.ircInviteChannel);
						self.irc.say(nick,
							sprintf('Inviting you to %s. Type `/join %s` (no quotes) to join.',
									self.ircInviteChannel, self.ircInviteChannel)
						);
						self.db.storeNickForUser(nick, userName, function(err, result) {
							if(err) {
								self.irc.say(self.ircStaffChannel, 'Error storing ' + nick + ' ' + userName + ' mapping');
							}
						})
					}
				}
			});
		}
	});

	self.initDatabase = function(cb) {
		self.db.init(cb);
	};

	self.go = function() {
		self.db.connect();
		self.irc.connect();
	};

	return self;
}
