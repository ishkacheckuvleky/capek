var fs = require('fs');
var capek = require('../');
var requiredIrcConfigKeys = ["ircNick", "ircInviteChannel", "ircStaffChannel",
"ircHostname", "ircPort", "ircNickservPassword"];

var requiredDbConfigKeys = ["dbDatabase"];


function main() {
	var argv = require('minimist')(process.argv.slice(2));
	var configFile = argv.config;
	if(!configFile) {
		usage();
		process.exit(1);
	}
	var configData = parseConfig(configFile);
	var bot = new capek.capekBot(configData);
	if(argv.hasOwnProperty("initdb")) {
		bot.initDatabase(function(err, succeeded) {
			if(err) {
				throw err;
			} else if(!succeeded) {
				console.log("didn't succeed but no error");
				process.exit(-1);
			}
		});
	}
	bot.go();
}

function parseConfig (path) {
	var fileContents = fs.readFileSync(path);
	var configData = JSON.parse(fileContents);
	requiredIrcConfigKeys.forEach(function(e, i, a) {
		if(!configData.irc.hasOwnProperty(e)) {
			throw new Error("Missing key " + e + " in IRC config");
		}
	});
	if(!configData.db.hasOwnProperty("dbHostname") &&
		!configData.db.hasOwnProperty("dbSocketPath")) {
			throw new Error("No hostname or socket path given for mysql");
		}
	return configData;
}

function usage() {
	console.log("Usage: capek --config configFile.json [--initdb]");
}

main();
